import * as mongoose from 'mongoose';
import { Player, IPlayer } from './players';

const mongoConfig: any = require('../../config/mongo.config.json')
const initialData: any[] = require('./players.json');

mongoose.connect(mongoConfig.connectionString);
console.log(`Connetcted to: ${mongoConfig.connectionString}`);

console.log('Remove players collection');
Player.remove({}, (err: any) => {
    if (err) {
        console.log(`Error: ${err}`);
        process.exit();
    }
    console.log('Players collection removed');
});

let counter: number = initialData.length;
initialData.forEach((player: any) => {
    let newPlayer = new Player();
    newPlayer.name = player.name;
    newPlayer.photoUrl = player.photoUrl;
    newPlayer.bio = player.bio;
    newPlayer.band = player.band;
    newPlayer.votes = player.votes;

    newPlayer.save(function (err) {
        counter--;
        if (err)
            console.log(`Error: ${player.name}, ${err}`);

        console.log(`Saved: ${player.name}`);

        if (counter == 0) {
            console.log('Finished');
            process.exit();
        }
    });
});