
import * as mongoose from 'mongoose';

export interface IPlayer extends mongoose.Document {
    name: string;
    bio: string;
    photoUrl: string;
    band?: string;
    votes: number;
}

export const PlayerSchema = new mongoose.Schema({
    name: { type: String, required: true },
    bio: { type: String, required: true },
    photoUrl: { type: String, required: true },
    band: String,
    votes: Number
});

export const Player = mongoose.model<IPlayer>('Player', PlayerSchema);