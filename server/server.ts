import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import * as mongoose from 'mongoose';

import { Player, IPlayer } from './db/players';

const mongoConfig: any = require('../config/mongo.config.json');

export class Server {

    private _app: express.Application;
    private _router: express.Router;
    private _connectionString = mongoConfig.connectionString;

    private constructor() {
        this._app = express();
        this._router = express.Router();
        this.config();
        this.api();
        mongoose.connect(mongoConfig.connectionString);
    }

    public get app(): express.Application {
        return this._app;
    }

    public static bootstrap(): Server {
        return new Server();
    }

    private api(): void {
        this._router.get('/', (req: express.Request, res: express.Response) => {
            res.json({ message: 'Welcome to our api!' });
        });

        this._router.get('/players', (req: express.Request, res: express.Response) => {

            let callback = (err: any, players: IPlayer[]) => {
                if (err)
                    res.send(err);

                res.json(players);
            };

            if (req.query.name) {
                Player.find({ name: new RegExp(req.query.name, 'i') }, callback);
            } else {
                Player.find(callback);
            }
        });


        this._router.route('/players/:id')
            .get((req: express.Request, res: express.Response) => {
                if (Number.isInteger(req.params.id))
                    res.send(400);

                Player.findById(req.params.id, (err: any, player: IPlayer) => {
                    if (err)
                        res.send(err);

                    res.json(player);
                });
            })
            .put((req: express.Request, res: express.Response) => {

                Player.findById(req.params.id, (err: any, player: IPlayer) => {
                    if (err)
                        res.send(err);

                    let data = req.body as IPlayer;
                    Object.assign(player, data);

                    player.save((err: any, updatedPlayer: IPlayer) => {
                        if (err)
                            res.send(err);

                        res.json(updatedPlayer);
                    });
                });
            });

        this._app.use('/api', this._router);
    }

    private config(): void {
        this._app.use(logger('dev'));
        this._app.use(bodyParser.urlencoded({ extended: true }));
        this._app.use(bodyParser.json());
        this._app.use(express.static(path.join(__dirname, '../dist')));
    }
}