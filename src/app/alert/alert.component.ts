import { Component, Input, Output, EventEmitter } from '@angular/core';

import { AlertModel, AlertType } from '../shared/alert.model';

@Component({
    selector: 'my-alert',
    templateUrl: './alert.component.html'
})
export class AlertComponent {

    @Input() alert: AlertModel;
    @Output() onDimissed = new EventEmitter<void>();

    dismiss() {
        this.onDimissed.emit();
    }
}