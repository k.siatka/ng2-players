import { Component } from '@angular/core';

import '../styles.global.scss';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})
export class AppComponent { }