import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//import './rxjs.extensions';

import { AppRoutingModule } from './app-routing.module';
import { PlayersService } from './shared/players.service';
import { ContactService } from './shared/contact.service';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PlayersComponent } from './players/players.component';
import { PlayerComponent } from './player/player.component';
import { PlayerListComponent } from './player-list/player-list.component';
import { PlayerTableComponent } from './player-table/player-table.component';
import { PlayerDetailsComponent } from './player-details/players-details.component';
import { PlayerSearchComponent } from './player-search/player-search.component';
import { PlayerVoteButtonComponent } from './player-vote-button/player-vote-button.component';
import { ContactComponent } from './contact/contact.component';
import { AlertComponent } from './alert/alert.component';
import { PageNotFoundComponent } from './not-found/not-found.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    PlayersService,
    ContactService
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    PlayersComponent,
    PlayerComponent,
    PlayerListComponent,
    PlayerTableComponent,
    PlayerDetailsComponent,
    PlayerSearchComponent,
    PlayerVoteButtonComponent,
    ContactComponent,
    AlertComponent,
    PageNotFoundComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

