import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { Observable } from 'rxjs/Observable';

import { ContactService } from '../shared/contact.service';
import { ContactModel } from '../shared/contact.model';
import { AlertModel, AlertType } from '../shared/alert.model';

@Component({
    templateUrl: './contact.component.html'
})
export class ContactComponent implements OnInit {

    emailPattern: string = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    contact: ContactModel = new ContactModel();
    topics: Observable<string[]>;
    isTopicInvalid: boolean = false;
    alert: AlertModel;

    constructor(
        private _titleService: Title,
        private _contactService: ContactService) { }

    ngOnInit() {
        this._titleService.setTitle('Contact');
        this.topics = this._contactService.getTopics();
    }

    submit(form: NgForm) {
        this.validateTopic(this.contact.topic);
        if (this.isTopicInvalid)
            return;

        this._contactService.send(this.contact)
            .subscribe(
                value => {
                    form.resetForm();
        
                    this.alert = new AlertModel(value);
                    this.contact = new ContactModel();
                    setTimeout(() => this.cancelAlert(), 5000);
                },
                error => {
                    this.alert = new AlertModel(error, AlertType.Danger, false);
                }
            );
    }

    cancelAlert() {
        this.alert = null;
    }

    validateTopic(value: string) {
        this.isTopicInvalid = value === 'default';
    }
}