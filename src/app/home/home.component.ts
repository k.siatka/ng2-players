import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    topVotedPlayers: Observable<PlayerModel[]>;

    constructor(
        private _titleService: Title,
        private _router: Router,
        private _playersService: PlayersService) { }

    ngOnInit() {
        this._titleService.setTitle('Home');

        this.topVotedPlayers = this._playersService.getTopPlayers();
    }

    selectPlayer($event: PlayerModel): void {
        console.log($event);
        this._router.navigate(['/player', $event._id]);
    }
}