import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'my-home',
    templateUrl: './not-found.component.html'
})
export class PageNotFoundComponent implements OnInit {

    constructor(private _titleService: Title) { }

    ngOnInit() {
        this._titleService.setTitle('Page not found');
    }
}