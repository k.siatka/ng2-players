import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    templateUrl: './players-details.component.html'
})
export class PlayerDetailsComponent implements OnInit {

    player: PlayerModel = new PlayerModel();

    constructor(
        private _titleService: Title,
        private route: ActivatedRoute,
        private _playersService: PlayersService) { }

    ngOnInit() {
        let playerId = this.route.snapshot.params['id'];
        if (!playerId) {
            return;
        }

        this._playersService.getPlayer(playerId)
            .subscribe(player => {
                this.player = player;
                this._titleService.setTitle('Player ' + this.player.name);
            });
    }

    voted(votes: number) {
        this.player.votes = votes;
    }
}