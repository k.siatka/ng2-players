import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player-list',
    templateUrl: './player-list.component.html',
    styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent {

    @Input() players: Observable<PlayerModel[]>;
    @Output() onPlayerSelected = new EventEmitter<PlayerModel>();

    selectPlayer(player: PlayerModel): void {
        this.onPlayerSelected.emit(player);
    }
}