import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player-search',
    templateUrl: './player-search.component.html',
    styleUrls: ['./player-search.component.scss']
})
export class PlayerSearchComponent implements OnInit {

    @Output() onResultSelected = new EventEmitter<PlayerModel>();

    players: Observable<PlayerModel[]>;
    private _searchTerms = new Subject<string>();

    constructor(private _playersService: PlayersService) { }

    search(term: string): void {
        this._searchTerms.next(term);
    }

    selected(player: PlayerModel): void {
        this.onResultSelected.emit(player);
    }

    ngOnInit() {
        this.players = this._searchTerms
            .debounceTime(300)        // wait for 300ms pause in events
            .distinctUntilChanged()   // ignore if next search term is same as previous
            .switchMap(term => term   // switch to new observable each time
                // return the http search observable
                ? this._playersService.search(term)
                // or the observable of empty heroes if no search term
                : Observable.of<PlayerModel[]>([]))
            .catch(error => {
                console.log(error);
                return Observable.of<PlayerModel[]>([]);
            });

    }

}