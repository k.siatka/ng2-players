import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player-table',
    templateUrl: './player-table.component.html',
    styleUrls: ['./player-table.component.scss']
})
export class PlayerTableComponent {

    @Input() players: Observable<PlayerModel[]>;
    @Output() onPlayerSelected = new EventEmitter<PlayerModel>();

    constructor(private _playersService: PlayersService) { }

    vote(player: PlayerModel) {
        player.votes++;
        this._playersService.update(player);
    }

    selectPlayer(player: PlayerModel): void {
        this.onPlayerSelected.emit(player);
    }
}