import { Component, EventEmitter, Input, Output } from '@angular/core';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player-vote-button',
    templateUrl: './player-vote-button.component.html'
})
export class PlayerVoteButtonComponent {

    @Input() player: PlayerModel;
    @Output() onVoted = new EventEmitter<number>();

    public isVoting = false;

    constructor(private _playersService: PlayersService) { }

    vote(inc: number) {
        this.isVoting = true;
        this.player.votes += inc;
        this._playersService.update(this.player)
            .delay(300)
            .subscribe(player => {
                this.isVoting = false;
                this.onVoted.emit(player.votes);
            });
    }
}