import { Component, EventEmitter, Input, Output } from '@angular/core';

import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player',
    templateUrl: './player.component.html',
    styles: ['img { cursor: pointer; }']
})
export class PlayerComponent {

    @Input() player: PlayerModel;
    @Output() onSelected = new EventEmitter<PlayerModel>();

    onSelect(player: PlayerModel): void {
        this.onSelected.emit(player);
    }

    voted(votes: number) {
        this.player.votes = votes;
    }
}