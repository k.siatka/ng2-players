import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    templateUrl: './players.component.html'
})
export class PlayersComponent implements OnInit {

    players: Observable<PlayerModel[]>;
    isListMode = true;

    constructor(
        private _playersService: PlayersService,
        private _router: Router,
        private _titleService: Title) { }

    ngOnInit() {
        this._titleService.setTitle('Players');
        this.players = this._playersService.getPlayers();
    }

    selectPlayer($event: PlayerModel): void {
        this._router.navigate(['/player', $event._id]);
    }

    toggleListMode() {
        this.isListMode = !this.isListMode;
    }
}