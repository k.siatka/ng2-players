export enum AlertType {
    Info,
    Success,
    Warning,
    Danger
}

export class AlertModel {

    constructor(
        public message: string = '',
        public type: AlertType = AlertType.Info,
        public isDismissible = true) { }

    get cssClass(): string {
        let css = 'alert alert-' + AlertType[this.type].toString().toLowerCase();
        if (this.isDismissible) {
            css += ' alert-dismissible';
        }
        return css;
    }
}