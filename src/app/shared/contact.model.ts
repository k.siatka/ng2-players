export class ContactModel {
    email: string;
    topic: string = 'default'; 
    message: string;
}