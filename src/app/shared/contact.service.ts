import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { getRandomInteger } from '../helper.extensions';

import { ContactModel } from './contact.model';

@Injectable()
export class ContactService {

    getTopics(): Observable<string[]> {
        return  Observable.of(['Question', 'Feedback', 'Support']);
    }

    send(contact: ContactModel): Observable<any> {
        console.log(`Submited ${JSON.stringify(contact)}`);

        if (getRandomInteger() % 2) {
            return Observable.of('Your message has been sent.');
        } else {
            return Observable.throw(new Error('Encounter some technical issues.'));
        }
    }
}
