export class PlayerModel {
    _id: string;
    name: string;
    votes: number;
    band?: string;
    photoUrl?: string;
    bio?: string;
}