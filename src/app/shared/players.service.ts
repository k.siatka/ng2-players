import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { PlayerModel } from './player.model';

@Injectable()
export class PlayersService {

    private _playersUrl = 'api/players';
    private _headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private _http: Http) { }

    getPlayers(): Observable<PlayerModel[]> {
        return this._http
            .get(this._playersUrl)
            .map(response => this.extractData<PlayerModel[]>(response))
            .catch(this.handleError);
    }

    getTopPlayers(limit: number = 3): Observable<PlayerModel[]> {
        return this.getPlayers()
            .map(result => {
                result.sort((a, b) => b.votes - a.votes);
                return result.slice(0, limit <= result.length ? limit : result.length);
            });
    }

    getPlayer(id: string): Observable<PlayerModel> {
        return this._http
            .get(`${this._playersUrl}/${id}`)
            .map(response => this.extractData<PlayerModel>(response))
            .catch(this.handleError);
    }

    search(term: string): Observable<PlayerModel[]> {
        return this._http
            .get(`${this._playersUrl}/?name=${term}`)
            .map(response => this.extractData<PlayerModel[]>(response))
            .catch(this.handleError);
    }

    update(player: PlayerModel): Observable<PlayerModel> {
        return this._http
            .put(`${this._playersUrl}/${player._id}`, JSON.stringify(player), { headers: this._headers })
            .map(response => this.extractData<PlayerModel>(response))
            .catch(this.handleError);
            // .toPromise()
            // .then(() => player)
            // .catch(error => {
            //     console.error('An error occurred', error);
            //     return error;
            // });
    }

    private extractData<T>(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        let body = res.json ? res.json() : null;
        return <T>(body != null ? body.data != null ? body.data : body : {});
    }

    private handleError(error: any): Observable<any> {
        console.error('An error occurred', error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
